extends Control

var accessibility_index = 0;	# This stores the index for the accessible HUD.
var accessibility_list = [];	# This stores all the accessibility text for the
								# accessible HUD.

var focused = false;			# If this is set to true, the focus has been
								# triggered at least once. This way, you can
								# read the name of the menu first, and then
								# later focuses will select the name of the
								# focus owner.

func _ready():
	# Grab the button we want to be focused first.
	$Button.grab_focus();

func _process(delta):
	var tts_triggered = false;
	if Input.is_action_pressed("access_key"):
		if Input.is_action_just_pressed("ui_up"):
			accessibility_index -= 1;
			tts_triggered = true
		if Input.is_action_just_pressed("ui_down"):
			accessibility_index += 1
			tts_triggered = true
			
		if accessibility_index >= accessibility_list.size():
			accessibility_index = 0
		elif accessibility_index < 0:
			accessibility_index = accessibility_list.size()-1
			
		if tts_triggered:
			TTS.speak(accessibility_list[accessibility_index])

# Updates the accessibility list text for the accessible HUD.
# This function should append a string to accessibility_list
# for each HUD element that you need to read, but can't be
# focused to.
func reset_accessibility_HUD_text():
	accessibility_list.append($Label.text)
	accessibility_list.append($Label2.text)
	accessibility_list.append($Label3.text)
	$Button.focus_neighbour_left = $Button2.get_path()

# This is triggered when a button enters focus. Each button has their
# focus_entered signal set to this method.
func _on_focus_entered():
	var button = get_focus_owner();
	
	if button is Button:
		TTS.speak(button.text)
